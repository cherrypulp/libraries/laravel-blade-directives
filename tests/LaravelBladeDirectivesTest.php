<?php

namespace Cherrypulp\LaravelBladeDirectives\Tests;

use Cherrypulp\LaravelBladeDirectives\Facades\LaravelBladeDirectives;
use Cherrypulp\LaravelBladeDirectives\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelBladeDirectivesTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-blade-directives' => LaravelBladeDirectives::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
