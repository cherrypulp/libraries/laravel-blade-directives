# Laravel Blade Directives

[![Build Status](https://travis-ci.org/cherrypulp/laravel-blade-directives.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-blade-directives)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/laravel-blade-directives/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/laravel-blade-directives/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/cherrypulp/laravel-blade-directives/badge.svg?branch=master)](https://coveralls.io/github/cherrypulp/laravel-blade-directives?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-blade-directives.svg)](https://packagist.org/packages/cherrypulp/laravel-blade-directives)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-blade-directives/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-blade-directives)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-blade-directives.svg)](https://packagist.org/packages/cherrypulp/laravel-blade-directives)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-blade-directives
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelBladeDirectives\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelBladeDirectives\Facades\LaravelBladeDirectives::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelBladeDirectives\ServiceProvider" --tag="config"
```

## Usage

CHANGE ME

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [](https://github.com/cherrypulp/laravel-blade-directives)
- [All contributors](https://github.com/cherrypulp/laravel-blade-directives/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
