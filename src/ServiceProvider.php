<?php

namespace Cherrypulp\LaravelBladeDirectives;

use Illuminate\Support\Facades\Blade;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->registerDirectives();
    }

    /**
     * Register all directives.
     *
     * @return void
     */
    public function registerDirectives()
    {
        $directives = require __DIR__ . '/directives.php';
        collect($directives)->each(function ($item, $key) {
            Blade::directive($key, $item);
        });
    }
}
